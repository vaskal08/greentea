//
//  Data.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/8/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Chat.h"
#import "Message.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@interface Data : NSObject{
    NSManagedObjectContext *context;
}

-(id)init;
-(void)addChatWithInfo:(NSDictionary *)info;
-(void)addMessageWithInfo:(NSDictionary *)info;
-(Chat *)getChatWithChatID:(NSString *)chatID;
-(NSArray *)getAllChats;
-(NSArray *)getMessagesWithChatID:(NSString *)chatID;

@end
