//
//  Tag.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/14/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface Tag : UIView{
    UITextView *tag;
}

@property NSString *tagText;

-(id)initWithOrigin:(CGPoint)origin andTagText:(NSString *)tagText;
-(id)initWithCenter:(CGPoint)center andTagText:(NSString *)tagText;

-(void)setText:(NSString *)text;
@end
