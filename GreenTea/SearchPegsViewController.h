//
//  SearchPegsViewController.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 7/13/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Tag.h"
#import <objc/runtime.h>
#import "Data.h"
#import "ViewController.h"

@interface UIView (StringTagAdditions)
@property (nonatomic, copy) NSString *stringTag;
@end

@implementation UIView (StringTagAdditions)
static NSString *kStringTagKey = @"StringTagKey";
- (NSString *)stringTag
{
    return objc_getAssociatedObject(self, CFBridgingRetain(kStringTagKey));
}
- (void)setStringTag:(NSString *)stringTag
{
    objc_setAssociatedObject(self, CFBridgingRetain(kStringTagKey), stringTag, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

@interface SearchPegsViewController : UIViewController{
    NSMutableDictionary *pegs;
    UIScrollView *scrollView;
}

@end