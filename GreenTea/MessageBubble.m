//
//  MessageBubble.m
//  College Chat
//
//  Created by Vasishta Kalinadhabhotla on 1/3/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "MessageBubble.h"


@implementation MessageBubble
@synthesize textView = _textView;
@synthesize message = _message;

-(id)initWithFrame:(CGRect)frame message:(NSString *)message andType:(MessageType)type{
    red = [UIColor colorWithRed:226.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1.0];
    gray = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
    //226, 106, 106
    //238, 238, 238
    //75, 119, 190
    //214, 69, 65
    self = [super initWithFrame:frame];
    if (self) {
        _textView = [[UITextView alloc]init];
        _textView.scrollEnabled = NO;
        _textView.font = [UIFont fontWithName:@"Avenir-Light" size:16];
        _textView.userInteractionEnabled = false;
        _textView.textColor = [UIColor blackColor];
        _textView.backgroundColor = [UIColor clearColor];
        _textView.text = message;
        
        CGFloat fixedWidth = 190;
        CGSize newSize = [_textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = _textView.frame;
        newFrame.origin = CGPointMake(newFrame.origin.x+5, newFrame.origin.y-1);
        newFrame.size = CGSizeMake(fminf(newSize.width, fixedWidth), newSize.height);
        _textView.frame = newFrame;
        
        _message = message;
        self.backgroundColor = [UIColor clearColor];
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _textView.frame.size.width+10, _textView.frame.size.height-10);
        
        if(type == kSelf){
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width-2, 0, 2, self.frame.size.height)];
            view.backgroundColor = red;
            [self addSubview:view];
            _textView.textAlignment = NSTextAlignmentRight;
        }
        else{
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 2, self.frame.size.height)];
            view.backgroundColor = gray;
            [self addSubview:view];
        }
        
        [self addSubview:_textView];
        messageType = type;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [self addGestureRecognizer:tap];
        
        
        origFrame = self.frame;
        open = false;
    }
    return self;
}
-(IBAction)tap:(id)sender{
    NSLog(@"tap bubble");
    if(!open){
        [UIView animateWithDuration:.5
                              delay:0
             usingSpringWithDamping:.5
              initialSpringVelocity:0
                            options:0 animations:^{
            CGFloat x=0;
            if(messageType == kOther){
                x=50;
            }else{
                x=-50;
            }
            self.frame = CGRectMake(self.frame.origin.x+x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        }completion:nil];
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(close:)
                                       userInfo:nil
                                        repeats:NO];
        
        open = true;
    }
    else{
        [self close:nil];
    }
}
-(IBAction)close:(id)sender{
    NSLog(@"close");
    if(open){
        [UIView animateWithDuration:.5
                              delay:0
             usingSpringWithDamping:.5
              initialSpringVelocity:0
                            options:0 animations:^{
            CGFloat x=0;
            if(messageType == kOther){
                x=-50;
            }else{
                x=50;
            }
            self.frame = CGRectMake(self.frame.origin.x+x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        }completion:nil];
        timer = nil;
        [timer invalidate];
        open = false;
    }
}

- (void)drawRect:(CGRect)rect {
    //self.layer.cornerRadius = fminf(self.frame.size.width/2, 15.0);
    //self.layer.masksToBounds = YES;
}


@end
