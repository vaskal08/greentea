//
//  Data.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/8/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "Data.h"

@implementation Data

-(id)init{
    self = [super init];
    if(self){
        context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    
    return self;
}

-(void)addChatWithInfo:(NSDictionary *)info{
    NSString *userID = [info objectForKey:@"userID"];
    NSString *recipientID = [info objectForKey:@"recipientID"];
    NSString *chatID = [info objectForKey:@"chatID"];
    NSString *title = [info objectForKey:@"title"];
    NSDate *lastModified = [info objectForKey:@"lastModified"];
    
    NSError *error;
    
    Chat *chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
    chat.userID = userID;
    chat.recipientID = recipientID;
    chat.chatID = chatID;
    chat.title = title;
    chat.lastModified = lastModified;
    
    [context save:&error];
}
-(void)addMessageWithInfo:(NSDictionary *)info{
    //only store 20 at a time for each chat
    
    NSString *userID = [info objectForKey:@"userID"];
    NSString *recipientID = [info objectForKey:@"recipientID"];
    NSString *chatID = [info objectForKey:@"chatID"];
    NSString *message = [info objectForKey:@"message"];
    NSDate *date = [info objectForKey:@"date"];
    NSNumber *read = [NSNumber numberWithBool:true];
    
    NSError *error;
    
    Message *m = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    m.userID = userID;
    m.recipientID = recipientID;
    m.chatID = chatID;
    m.message = message;
    m.date = date;
    m.read = read;
    
    [context save:&error];
    
    
    NSEntityDescription *session = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:session];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatID == %@", chatID];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSArray *messages = [context executeFetchRequest:request error:&error];

    for(int x = 0; x<[messages count]; x++){
        Message *mess = [messages objectAtIndex:x];
    }
    if([messages count]>20){
        int deletions = [messages count]-20;
        for (int x=0; x<deletions; x++) {
            Message *mess = [messages objectAtIndex:x];
            [context deleteObject:mess];
        }
    }
    
    
}
-(NSArray *)getMessagesWithChatID:(NSString *)chatID{
    NSError *error;
    NSEntityDescription *session = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:session];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chatID == %@", chatID];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSArray *messages = [context executeFetchRequest:request error:&error];
    return messages;
}
-(Chat *)getChatWithChatID:(NSString *)chatID{
    return [[Chat alloc]init];
}
-(NSArray *)getAllChats{
    NSArray *chats = [[NSArray alloc]init];
    
    NSError *error;
    NSEntityDescription *session = [NSEntityDescription entityForName:@"Chat" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:session];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastModified" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    chats = results;
    
    return chats;
}


/*
-(NSString *)getUserID{
    NSError *error;
    NSEntityDescription *session = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:session];
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    NSString *uid = [self uuid];
    
    if ([results count]==0) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"CheckingParseForUser" object:nil];
        
        while ([self userExists:uid]) {
            uid = [self uuid];
        }
        NSError *error;
        
        User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        user.userID = uid;
        
        [context save:&error];
        
        PFObject *u = [PFObject objectWithClassName:@"User"];
        u[@"userID"] = uid;
        UIDevice *d = [UIDevice currentDevice];
        u[@"device"] = [NSString stringWithFormat:@"%@ - %@ (%@)", [d model], [d systemName], [d systemVersion]];
        [u saveInBackground];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"EndCheckingParseForUser" object:nil];
        
        return uid;
    }
    else{
        User *user = [results objectAtIndex:0];
        return user.userID;
    }
}*/
/*
-(BOOL)userExists:(NSString *)userID{
    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"userID" equalTo:userID];
    
    NSArray *array = [query findObjects];
    
    return [array count]>0;
}*/

@end
