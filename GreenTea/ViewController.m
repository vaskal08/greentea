//
//  ViewController.m
//  College Chat
//
//  Created by Vasishta Kalinadhabhotla on 1/3/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "ViewController.h"
#import <Sinch/Sinch.h>
#import <Sinch/SINMessageClient.h>
#import "TouchDownGestureRecognizer.h"
#import "DAKeyboardControl.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize selfID = _selfID;
@synthesize otherID = _otherID;
@synthesize chatID = _chatID;
@synthesize chatRoom = _chatRoom;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sinchClient = [Sinch clientWithApplicationKey:@"011ae60e-5e3a-4a80-b1e2-b453bb2d52f0"
                                              applicationSecret:@"XH/esV6+mkahPeQ/3YSzlg=="
                                                environmentHost:@"sandbox.sinch.com"
                                                         userId:_selfID];
    
    [sinchClient setSupportMessaging:YES];
    
    sinchClient.delegate = self;
    
    initialLoad = false;
    editing = false;
    
    [sinchClient start];
    [sinchClient startListeningOnActiveConnection];

    messageClient = [sinchClient messageClient];
    messageClient.delegate = self;
    
    startDate = [NSDate date];
    
    keys = [[NSMutableArray alloc]init];
    
    end = false;
    
    
    NSLog(@"inital load: %s", initialLoad ? "true":"false");
    
    Firebase *ref = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://peg-data.firebaseio.com/%@", _chatID]];
    [ref observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        if(initialLoad){
            NSString *to = snapshot.value[@"to"];
            NSString *from = snapshot.value[@"from"];
            NSString *message = snapshot.value[@"message"];
            NSLog(@"key: %@", snapshot.key);
            
            if([to isEqualToString:_selfID]){
                [self recieveMessage:message];
            }
        }
    }];
    
    
    
    /*
    
    // Attach a block to read the data at our posts reference
    [ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        NSLog(@"%@", snapshot.value);
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];*/
    
    
    self.navigationController.navigationBarHidden = false;
    loading = false;
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    Firebase *ref = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://peg-data.firebaseio.com/%@", _chatID]];
    [[[ref queryOrderedByKey] queryLimitedToLast:30] observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        NSLog(@"key!: %@", snapshot.value);
        
        initialData = snapshot;
        
        for(FDataSnapshot *snap in snapshot.children){
            if(!initialLoad){
                [keys addObject:snap.key];
                NSString *to = snap.value[@"to"];
                NSString *from = snap.value[@"from"];
                NSString *message = snap.value[@"message"];
                NSLog(@"key: %@", snap.key);
                
                if([to isEqualToString:_selfID]){
                    [self recieveMessage:message];
                }
                else if([to isEqualToString:_otherID]){
                    [self sendMessage:message];
                }
            }
        }
        
        initialLoad = true;
    }];
}
-(NSDate *) toGlobalTimeFromTime:(NSDate*)time
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: time];
    return [NSDate dateWithTimeInterval: seconds sinceDate: time];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view removeKeyboardControl];
    initialLoad = false;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    keyboardShowing = false;
    
    bubbles = [[NSMutableArray alloc]init];
    
    UIColor *red = [UIColor colorWithRed:226.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard:)];
    TouchDownGestureRecognizer *touchdown = [[TouchDownGestureRecognizer alloc]initWithTarget:self action:@selector(touchDown:)];
    tap.cancelsTouchesInView = false;
    touchdown.cancelsTouchesInView = false;
    
    [self.navigationController.navigationBar setTintColor:red];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName : red,
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0]
                                                                      }];
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:scrollView];
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
    [scrollView setContentOffset:bottomOffset animated:NO];
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 20);
    scrollView.delegate = self;
    
    
    //[scrollView addGestureRecognizer:touchdown];
    [scrollView addGestureRecognizer:tap];
    
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        NSLog(@"opening %s", opening ? "true" : "false");
        NSLog(@"closing %s", closing ? "true" : "false");
        NSLog(@"%f", keyboardFrameInView.origin.y);
        CGRect toolBarFrame = toolbar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        toolbar.frame = toolBarFrame;
        
        CGRect tableViewFrame = scrollView.frame;
        tableViewFrame.size.height = toolBarFrame.origin.y;
        scrollView.frame = tableViewFrame;
    }];
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, 44)];
    [self.view addSubview:toolbar];
    
    textInput = [[UITextField alloc]initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-60, 34)];
    textInput.borderStyle = UITextBorderStyleRoundedRect;
    textInput.userInteractionEnabled = false;
    textInput.placeholder = @"Write a message...";
    [toolbar addSubview:textInput];
    
    textView = [[UITextView alloc]initWithFrame:textInput.frame];
    textView.backgroundColor = [UIColor clearColor];
    textView.font = [UIFont systemFontOfSize:16];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:nil];
    textView.delegate = self;
    [toolbar addSubview:textView];
    
    send = [UIButton buttonWithType:UIButtonTypeSystem];
    send.frame =CGRectMake(toolbar.frame.size.width-55, 5, 50, 34);
    [send setTintColor:red];
    [send setTitle:@"Send" forState:UIControlStateNormal];
    [send addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [send setEnabled:false];
    [toolbar addSubview:send];
    
    Data *data = [[Data alloc]init];
    //NSArray *messages = [data getMessagesWithChatID:_chatID];
    //skip = [messages count];
        /*
    for (Message *message in messages) {
        if ([message.userID isEqualToString:_selfID]) {
            [self sendMessage:message.message];
        }
        else{
            [self recieveMessage:message.message];
        }
    }
    */
}

-(void)textDidChange:(NSNotification *)notification{
    if([textView.text isEqualToString:@""]){
        [send setEnabled:false];
        textInput.placeholder = @"Write a message...";
    }
    else {
        [send setEnabled:true];
        textInput.placeholder = @"";
    }
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGFloat maxHeight = 100;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.origin = CGPointMake(newFrame.origin.x, newFrame.origin.y);
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    textView.frame = newFrame;
    textInput.frame = newFrame;
    
    toolbar.frame = CGRectMake(0, self.view.frame.size.height-(10+textInput.frame.size.height+1+keyboardRect.size.height), self.view.frame.size.width, 10+textInput.frame.size.height);
    scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-(keyboardRect.size.height+toolbar.frame.size.height));
    
    
    if(scrollView.contentSize.height>scrollView.frame.size.height){
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    
}

-(IBAction)exit:(id)sender{
    [sinchClient terminate];
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)hideKeyboard:(UITapGestureRecognizer *)gesture{
    keyboardShowing = false;
    [textView resignFirstResponder];
}
-(void)keyboardWillShow:(NSNotification *)notification {
    keyboardShowing = true;
    keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    [UIView animateWithDuration:.3 animations:^{
        toolbar.frame = CGRectMake(0, self.view.frame.size.height-(10+textInput.frame.size.height+keyboardRect.size.height), self.view.frame.size.width, 10+textInput.frame.size.height);
        scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-(keyboardRect.size.height+toolbar.frame.size.height));
    }];
    if(scrollView.contentSize.height>scrollView.frame.size.height){
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    
}
-(IBAction)keyboardWillHide:(id)sender{
    keyboardShowing = false;
    [UIView animateWithDuration:.3 animations:^{
        toolbar.frame = CGRectMake(0, self.view.frame.size.height-(10+textInput.frame.size.height), self.view.frame.size.width, 10+textInput.frame.size.height);
        scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-(toolbar.frame.size.height));
    }];
}
-(void)recieveMessage:(NSString*)message{
    
    MessageBubble *messageBubble = [[MessageBubble alloc]initWithFrame:CGRectMake(100, 100, 0, 0) message:message andType:kOther];
    
    CGFloat height = messageBubble.frame.size.height;
    
    messageBubble.frame = CGRectMake(15, scrollView.contentSize.height-10, messageBubble.frame.size.width, messageBubble.frame.size.height);
    messageBubble.alpha = 0;
    [scrollView addSubview:messageBubble];
    
    
    [bubbles addObject:messageBubble];
    
    [UIView animateWithDuration:.2 animations:^{
        messageBubble.alpha = 1.0;
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+height);
    }];
    
    if(scrollView.contentSize.height>scrollView.frame.size.height){
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    
    
}
-(void)sendMessage:(NSString *)message{
    MessageBubble *messageBubble = [[MessageBubble alloc]initWithFrame:CGRectMake(100, 100, 0, 0) message:message andType:kSelf];
    
    CGFloat height = messageBubble.frame.size.height;
    
    messageBubble.frame = CGRectMake(scrollView.contentSize.width-(15+messageBubble.frame.size.width), scrollView.contentSize.height-10, messageBubble.frame.size.width, messageBubble.frame.size.height);
    [scrollView addSubview:messageBubble];
    messageBubble.alpha = 0;
    
    [bubbles addObject:messageBubble];
    
    [UIView animateWithDuration:.2 animations:^{
        messageBubble.alpha = 1.0;
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+height);
    }];
    
    textView.text = @"";
    [send setEnabled:false];
    textInput.placeholder = @"Write a message...";
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGFloat maxHeight = 100;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.origin = CGPointMake(newFrame.origin.x, newFrame.origin.y);
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    textView.frame = newFrame;
    textInput.frame = newFrame;
    
    toolbar.frame = CGRectMake(0, self.view.frame.size.height-(10+textInput.frame.size.height+1+keyboardRect.size.height), self.view.frame.size.width, 10+textInput.frame.size.height);
    scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-(keyboardRect.size.height+toolbar.frame.size.height));
    
    if(scrollView.contentSize.height>scrollView.frame.size.height){
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
}
-(IBAction)send:(id)sender{
    NSString *message = textView.text;
    message = [message stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *sMessage = [message stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *sendingInfo = [NSString stringWithFormat:@"{\"chatID\":\"%@\", \"message\":\"%@\"}", _chatID, sMessage];
    
    MessageBubble *messageBubble = [[MessageBubble alloc]initWithFrame:CGRectMake(100, 100, 0, 0) message:message andType:kSelf];
    
    CGFloat height = messageBubble.frame.size.height;
    
    messageBubble.frame = CGRectMake(scrollView.contentSize.width-(15+messageBubble.frame.size.width), scrollView.contentSize.height-10, messageBubble.frame.size.width, messageBubble.frame.size.height);
    [scrollView addSubview:messageBubble];
    messageBubble.alpha = 0;
    
    [bubbles addObject:messageBubble];
    
    [UIView animateWithDuration:.2 animations:^{
        messageBubble.alpha = 1.0;
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+height);
    }];
    
    textView.text = @"";
    [send setEnabled:false];
    textInput.placeholder = @"Write a message...";
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGFloat maxHeight = 100;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.origin = CGPointMake(newFrame.origin.x, newFrame.origin.y);
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    textView.frame = newFrame;
    textInput.frame = newFrame;
    
    toolbar.frame = CGRectMake(0, self.view.frame.size.height-(10+textInput.frame.size.height+1+keyboardRect.size.height), self.view.frame.size.width, 10+textInput.frame.size.height);
    scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-(keyboardRect.size.height+toolbar.frame.size.height));
    
    if(scrollView.contentSize.height>scrollView.frame.size.height){
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    
    PFObject *sendingMessage = [PFObject objectWithClassName:@"Message"];
    sendingMessage[@"message"] = message;
    sendingMessage[@"from"] = _selfID;
    sendingMessage[@"to"] = _otherID;
    sendingMessage[@"chatID"] = _chatID;
    [sendingMessage saveInBackground];
    
    /*
    SINOutgoingMessage *outgoingMessage = [SINOutgoingMessage messageWithRecipient:_otherID text:sendingInfo];
    [messageClient sendMessage:outgoingMessage];
    skip++;
     */
    Firebase *ref = [[Firebase alloc] initWithUrl: @"https://peg-data.firebaseio.com/"];
    
    ref = [ref childByAppendingPath: _chatID];
    NSDictionary *post1 = @{
                            @"from": _selfID,
                            @"to": _otherID,
                            @"message" :  message
                            };
    Firebase *post1Ref = [ref childByAutoId];
    [post1Ref setValue: post1];
    
    
    
    
    Data *data = [[Data alloc]init];
    NSDictionary *info = [[NSDictionary alloc]initWithObjectsAndKeys:message, @"message", _selfID, @"userID", _otherID, @"recipientID", _chatID, @"chatID", [NSDate date], @"date", nil];
    
    [data addMessageWithInfo:info];

}

//message delegate

-(void)clientDidStart:(id<SINClient>)client{
    
}
-(void)clientDidStop:(id<SINClient>)client{
    
}
-(void)clientDidFail:(id<SINClient>)client error:(NSError *)error{
    
}
- (void) messageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId{
    // Persist outgoing message
    // Update UI
    NSLog(@"message sent");
}
- (void) messageDelivered:(id<SINMessageDeliveryInfo>)info {
    NSLog(@"Message with id %@ was delivered to recipient with id  %@",
          info.messageId,
          info.recipientId);
}
- (void) messageDeliveryFailed:(id<SINMessage>) message info:(NSArray *)messageFailureInfo {
    for (id<SINMessageFailureInfo> reason in messageFailureInfo) {
        NSLog(@"Delivering message with id %@ failed to user %@. Reason %@",
              reason.messageId, reason.recipientId, [reason.error localizedDescription]);
    }
}
-(void)messageFailed:(id<SINMessage>)message info:(id<SINMessageFailureInfo>)messageFailureInfo{
    NSLog(@"message failed: %@", [messageFailureInfo.error localizedDescription]);
}

- (void) messageClient:(id<SINMessageClient>) messageClient didReceiveIncomingMessage:(id<SINMessage>)message {
    // Present a Local Notification if app is in background
    
    NSLog(@"incoming message");
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){

    } else {
        skip++;
        NSString *json = [message text];
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                                              options:0 error:NULL];
        NSString *m = [jsonObject objectForKey:@"message"];
        NSString *cid = [jsonObject objectForKey:@"chatID"];
        
        if([cid isEqualToString:_chatID]){
            if([[NSDate date] timeIntervalSinceDate:startDate] > 5){
                int b = [m integerValue];
                [self handleTypingIndicatorWithStatus:b];
                NSLog(@"%i", b);
            }
        }
    }
    // Persist incoming message
}

-(void)handleTypingIndicatorWithStatus:(NSInteger )status{
    if(status==0){
        
    }
    else if(status ==1){
        NSString *pathForGif = [[NSBundle mainBundle] pathForResource: @"indicator" ofType: @"gif"];
        
        NSData *gifData = [NSData dataWithContentsOfFile: pathForGif];
                
        FLAnimatedImage *image = [[FLAnimatedImage alloc]initWithAnimatedGIFData:gifData];
        indicator = [[FLAnimatedImageView alloc] init];
        indicator.animatedImage = image;
        indicator.frame = CGRectMake(15, scrollView.contentSize.height, 50, 30.0);
        [self.view addSubview:indicator];
        
        [scrollView addSubview:indicator];
        
        [UIView animateWithDuration:.2 animations:^{
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+indicator.frame.size.height);
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+30)];
        }];
        
        if(scrollView.contentSize.height>scrollView.frame.size.height){
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            [scrollView setContentOffset:bottomOffset animated:YES];
        }
        
        
        
    }
}

-(void)loadNewMessage:(NSString *)message from:(NSString *)from to:(NSString *)to{
    CGFloat h = 0;
    //for(PFObject *m in messages){
        
        message = [message stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        MessageBubble *messageBubble;
        if([from isEqualToString:_selfID]){
            messageBubble= [[MessageBubble alloc]initWithFrame:CGRectMake(100, 100, 0, 0) message:message andType:kSelf];
            messageBubble.frame = CGRectMake(scrollView.contentSize.width-(15+messageBubble.frame.size.width), 0, messageBubble.frame.size.width, messageBubble.frame.size.height);
        }
        else{
            messageBubble= [[MessageBubble alloc]initWithFrame:CGRectMake(100, 100, 0, 0) message:message andType:kOther];
            messageBubble.frame = CGRectMake(15, 0, messageBubble.frame.size.width, messageBubble.frame.size.height);
        }
        
        CGFloat height = messageBubble.frame.size.height;
        
        scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height+height);
        for (MessageBubble *bubble in bubbles) {
            bubble.frame = CGRectMake(bubble.frame.origin.x, bubble.frame.origin.y+height, bubble.frame.size.width, bubble.frame.size.height);
        }
        
        [scrollView addSubview:messageBubble];
        [bubbles insertObject:messageBubble atIndex:0];
        h += height;
    //}
    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height);
    for (MessageBubble *bubble in bubbles) {
        bubble.frame = CGRectMake(bubble.frame.origin.x, bubble.frame.origin.y, bubble.frame.size.width, bubble.frame.size.height);
    }
    
    scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y+height);
}

-(void)scrollViewDidScroll: (UIScrollView*)sView
{
    //float scrollViewHeight = scrollView.frame.size.height;
    //float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = sView.contentOffset.y;
    
    /*
    float diff = scrollOffset-startY;
    NSLog(@"%f", diff);
    
    UIView * activeKeyboard = textView.inputAccessoryView.superview;
    if(diff<0){
        //activeKeyboard.frame = CGRectMake(activeKeyboard.frame.origin.x, startKeyboardY+diff, activeKeyboard.frame.size.width, activeKeyboard.frame.size.height);
    }
    */
    
    
    
    if (scrollOffset == 0){
        NSLog(@"at top");
        if(!loading && !end){
            loading=true;
            NSString *startKey = [keys objectAtIndex:0];
            Firebase *ref = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://peg-data.firebaseio.com/%@", _chatID]];
            [[[[ref queryOrderedByKey] queryEndingAtValue:startKey] queryLimitedToLast:21] observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                NSLog(@"key!: %@", snapshot.value);
                
                int c=0;
                NSMutableArray *children = [[NSMutableArray alloc]init];
                
                for(FDataSnapshot *snap in snapshot.children){
                    [children addObject:snap];
                }
                
                if([children count]==1){
                    end = true;
                }
                
                for(int x=[children count]-1; x>=0; x--){
                    FDataSnapshot *snap = [children objectAtIndex:x];
                    if(c!=0){
                        [keys insertObject:snap.key atIndex:0];
                        NSString *to = snap.value[@"to"];
                        NSString *from = snap.value[@"from"];
                        NSString *message = snap.value[@"message"];
                        NSLog(@"key: %@", snap.key);
                        
                        [self loadNewMessage:message from:from to:to];
                    }
                    c++;
                }
                loading= false;
                if(!end)scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y-30);
                
            }];
        }
    }
}



- (void)textViewDidChange:(UITextView *)textView{
    NSLog(@"started editing");
    [editor invalidate];
    editor = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(endEditing) userInfo:nil repeats:NO];
    
    if(!editing){
        NSString *sendingInfo = [NSString stringWithFormat:@"{\"chatID\":\"%@\", \"message\":\"%i\"}", _chatID, 1];
        
        SINOutgoingMessage *outgoingMessage = [SINOutgoingMessage messageWithRecipient:_otherID text:sendingInfo];
        
        [messageClient sendMessage:outgoingMessage];
        editing = true;
        
    }
}
- (void)endEditing{
    NSLog(@"ended editing");
    editing = false;
    NSString *sendingInfo = [NSString stringWithFormat:@"{\"chatID\":\"%@\", \"message\":\"%i\"}", _chatID, 0];
    
    SINOutgoingMessage *outgoingMessage = [SINOutgoingMessage messageWithRecipient:_otherID text:sendingInfo];
    
    [messageClient sendMessage:outgoingMessage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
