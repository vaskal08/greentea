//
//  Chat.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "Chat.h"


@implementation Chat

@dynamic userID;
@dynamic recipientID;
@dynamic chatID;
@dynamic title;
@dynamic lastModified;

@end
