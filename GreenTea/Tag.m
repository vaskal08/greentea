//
//  Tag.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/14/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "Tag.h"

@implementation Tag

-(id)initWithOrigin:(CGPoint)origin andTagText:(NSString *)tagText{
    self = [super init];
    if(self){
        tag = [[UITextView alloc]init];
        tag.backgroundColor = [UIColor clearColor];
        tag.textColor = [UIColor whiteColor];
        tag.text = tagText;
        tag.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        tag.textAlignment = NSTextAlignmentCenter;
        tag.userInteractionEnabled = false;
        
        CGFloat fixedWidth = tag.frame.size.width;
        CGSize newSize = [tag sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = tag.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        tag.frame = newFrame;
        tag.frame = CGRectMake(10, 0, newFrame.size.width, newFrame.size.height);
        
        [self addSubview:tag];
        
        self.frame = CGRectMake(origin.x, origin.y, tag.frame.size.width+20, tag.frame.size.height);
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(id)initWithCenter:(CGPoint)center andTagText:(NSString *)tagText{
    self = [super init];
    if(self){
        tag = [[UITextView alloc]init];
        tag.backgroundColor = [UIColor clearColor];
        tag.textColor = [UIColor whiteColor];
        tag.text = tagText;
        tag.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        tag.textAlignment = NSTextAlignmentCenter;
        tag.userInteractionEnabled = false;
        
        CGFloat fixedWidth = tag.frame.size.width;
        CGSize newSize = [tag sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = tag.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        tag.frame = newFrame;
        tag.frame = CGRectMake(15, 0, newFrame.size.width+5, newFrame.size.height);
        
        [self addSubview:tag];
        
        self.frame = CGRectMake(center.x, center.y, tag.frame.size.width+30, tag.frame.size.height);
        self.center = center;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)setText:(NSString *)text{
    tag.text = text;
}
- (void)drawRect:(CGRect)rect {
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 2.0f;
    self.backgroundColor = [UIColor clearColor];
    self.layer.masksToBounds = true;
    self.layer.cornerRadius = self.frame.size.height/2;
}


@end
