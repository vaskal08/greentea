//
//  MessageBubble.h
//  College Chat
//
//  Created by Vasishta Kalinadhabhotla on 1/3/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum MessageType : NSUInteger {
    kSelf,
    kOther
} MessageType;
@interface MessageBubble : UIView{
    UIColor *red;
    UIColor *gray;
    MessageType messageType;
    CGRect origFrame;
    bool open;
    NSTimer *timer;
}

-(id)initWithFrame:(CGRect)frame message:(NSString *)message andType:(MessageType)type;

@property NSString *message;
@property UITextView *textView;

@end
