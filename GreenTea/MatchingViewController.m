//
//  MatchingViewController.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/14/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "MatchingViewController.h"

#define ARC4RANDOM_MAX      0x100000000

@interface MatchingViewController ()

@end

@implementation MatchingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    double val = ((double)arc4random() / ARC4RANDOM_MAX);
    val+=2;
    NSLog(@"%f", val);
    left = false;

    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(terminatingApplication:) name:@"TerminatingApplication" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(terminatingApplication:) name:@"ApplicationEnteringBackground" object:nil];
    
    NSString *pathForGif = [[NSBundle mainBundle] pathForResource: @"loading" ofType: @"gif"];
    
    NSData *gifData = [NSData dataWithContentsOfFile: pathForGif];
    
    FLAnimatedImage *image = [[FLAnimatedImage alloc]initWithAnimatedGIFData:gifData];
    FLAnimatedImageView *imageView = [[FLAnimatedImageView alloc] init];
    imageView.animatedImage = image;
    imageView.frame = CGRectMake((self.view.frame.size.width-300)/2, (self.view.frame.size.height-400)/2, 300.0, 300.0);
    [self.view addSubview:imageView];
    
    self.navigationController.navigationBarHidden = true;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-(94+44), self.view.frame.size.width, 44)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Finding someone for you!";
    [self.view addSubview:label];
    
    UIButton *cancel = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancel addTarget:self
               action:@selector(cancel:)
     forControlEvents:UIControlEventTouchUpInside];
    [cancel setTitleColor:[UIColor colorWithRed:(239.0/255.0) green:(72.0/255.0) blue:(54.0/255.0) alpha:1] forState:UIControlStateNormal];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    cancel.frame = CGRectMake(0, self.view.frame.size.height-94, self.view.frame.size.width, 64);
    [self.view addSubview:cancel];
    
    
}
-(IBAction)cancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    [matchToken deleteInBackground];
    [timer invalidate];
}
-(void)terminatingApplication:(NSNotification *)notification{
    if(!left){
        NSLog(@"terminating");
        [self dismissViewControllerAnimated:NO completion:^{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Hoopla was interrupted while matching!" message:@"Try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        }];

        [matchToken deleteInBackground];
        [timer invalidate];
    }
}

/*
-(void)update{
    NSLog(@"updating");
    Data *data = [[Data alloc]init];
    if(matchToken){
    [PFCloud callFunctionInBackground:@"search" withParameters:@{@"userID": [data getUserID]}
        block:^(NSArray *results, NSError *error) {
            if (!error){
                
                [PFCloud callFunctionInBackground:@"check" withParameters:@{@"userID": [data getUserID]}
                    block:^(PFObject *result, NSError *error) {
                        if (!error){
                            
                            if(![result isEqual:@"no elements"]){
                                NSLog(@"%@", result);
                                NSString *selfID = [data getUserID];
                                
                                NSArray *members = result[@"members"];
                                NSString *otherID;
                                if([[members objectAtIndex:0] isEqualToString:selfID]){
                                    otherID = [members objectAtIndex:1];
                                }
                                else{
                                    otherID = [members objectAtIndex:0];
                                }
                                [self join:result withSelfID:selfID andOtherID:otherID];
                                [matchToken deleteInBackground];
                                
                            }
                            else{
                                if([results count]>0){
                                    int length = [results count];
                                    int rand = arc4random_uniform(length);
                                    
                                    PFObject *token = [results objectAtIndex:rand];
                                    NSString *otherID = token[@"userID"];
                                    NSLog(@"%@", otherID);
                                    NSString *selfID = [data getUserID];
                                    
                                    [PFCloud callFunctionInBackground:@"make" withParameters:@{@"userID": [data getUserID], @"otherID": otherID}
                                        block:^(PFObject *chatRoom, NSError *error) {
                                            if (!error){
                                                if(![chatRoom isEqual:@"false"]){
                                                    [self join:chatRoom withSelfID:selfID andOtherID:otherID];
                                                }
                                            }
                                    }];
                                }
                            }
                                                                            
                        }
                    }];
                                        
                                        
                }
        }];
    }
}
 */
-(void)join:(PFObject *)chatRoom withSelfID:(NSString *)selfID andOtherID:(NSString *)otherID{
    [timer invalidate];
    ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatController"];
    controller.selfID = selfID;
    controller.otherID = otherID;
    controller.chatID = chatRoom[@"chatID"];
    controller.chatRoom = chatRoom;
    left = true;
    [self.navigationController pushViewController:controller animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
