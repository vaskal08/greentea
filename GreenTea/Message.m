//
//  Message.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic message;
@dynamic read;
@dynamic userID;
@dynamic recipientID;
@dynamic chatID;
@dynamic date;

@end
