//
//  ChatsViewController.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "ChatsViewController.h"

@interface ChatsViewController ()

@end

@implementation ChatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.contentSize = self.view.frame.size;
    
    Data *data = [[Data alloc]init];
    
    chats = [data getAllChats];
    
    NSLog(@"%i", [chats count]);
    CGFloat height = 200;
    for(int x= 0; x<[chats count]; x++){
        Chat *chat = [chats objectAtIndex:x];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.tag = x;
        [button addTarget:self action:@selector(goToChat:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[NSString stringWithFormat:@"%@", chat.title] forState:UIControlStateNormal];
        button.frame = CGRectMake(80.0, 100+(40*x), 160.0, 40.0);
        [scrollView addSubview:button];
        height+=40;
    }
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, height);
    [self.view addSubview:scrollView];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}
-(IBAction)goToChat:(id)sender{
    NSInteger x = [sender tag];
    
    Chat *chat = [chats objectAtIndex:x];
    
    ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatController"];
    controller.selfID = chat.userID;
    controller.otherID = chat.recipientID;
    controller.chatID = chat.chatID;
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
