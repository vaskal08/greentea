//
//  MainViewController.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/5/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize username, to, chatid, chatTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Firebase defaultConfig].persistenceEnabled = YES;
    
    tagList = [[NSMutableArray alloc]init];
    tagButtonList = [[NSMutableArray alloc]init];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    changed = false;
    
    PFUser *currentUser = [PFUser currentUser];
    
    if(!currentUser){
        [PFAnonymousUtils logInWithBlock:^(PFUser *user, NSError *error) {
            if (error) {
                NSLog(@"Anonymous login failed.");
            } else {
                NSLog(@"%@", user.objectId);
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation setObject:[PFUser currentUser].objectId forKey:@"userID"];
                
                [currentInstallation saveInBackground];
            }
        }];
    }
    else{
        NSLog(@"%@", currentUser.objectId);
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        [currentInstallation setObject:[PFUser currentUser].objectId forKey:@"userID"];
        
        [currentInstallation saveInBackground];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    Data *data = [[Data alloc]init];
    
    self.view.backgroundColor = [UIColor colorWithRed:191.0/255.0 green:85.0/255.0 blue:236.0/255.0 alpha:1];
    
    addP = [[Tag alloc]initWithCenter:CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height/2)-45) andTagText:@"Create a Peg"];
    [self.view addSubview:addP];
    
    NSLog(@"height: %f", addP.frame.size.height);
    
    viewP = [[Tag alloc]initWithCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2) andTagText:@"Search Pegs"];
    [self.view addSubview:viewP];
    
    viewC = [[Tag alloc]initWithCenter:CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height/2)+45) andTagText:@"My Chats"];
    [self.view addSubview:viewC];
    
    UIButton *createPeg = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createPeg addTarget:self
                  action:@selector(createPeg:)
        forControlEvents:UIControlEventTouchUpInside];
    createPeg.frame = CGRectMake(0, 0, addP.frame.size.width, addP.frame.size.height);
    [addP addSubview:createPeg];
    
    UIButton *viewPegs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [viewPegs addTarget:self
                 action:@selector(viewPegs:)
       forControlEvents:UIControlEventTouchUpInside];
    viewPegs.frame = CGRectMake(0, 0, viewP.frame.size.width, viewP.frame.size.height);
    [viewP addSubview:viewPegs];
    
    UIButton *viewChats = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [viewChats addTarget:self
                 action:@selector(chats:)
       forControlEvents:UIControlEventTouchUpInside];
    viewChats.frame = CGRectMake(0, 0, viewC.frame.size.width, viewC.frame.size.height);
    [viewC addSubview:viewChats];
    
}
-(IBAction)publishPeg:(id)sender{
    NSString *titleText = title.text;
    NSString *desc = description.text;
    NSArray *array = [NSArray arrayWithArray:tagList];
    PFUser *currentUser = [PFUser currentUser];
    
    PFObject *peg = [PFObject objectWithClassName:@"Peg"];
    peg[@"title"] = titleText;
    peg[@"description"] = desc;
    peg[@"tags"] = array;
    peg[@"userID"] = currentUser.objectId;
    [peg saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self createPeg:nil];
        } else {
            // There was a problem, check error.description
        }
    }];
    
}
-(void)textViewDidChange:(UITextView *)textView{
    NSString *text = description.text;
    if ([text isEqualToString:@""]) {
        placeholder.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"provide a brief description" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    }
    else{
        placeholder.placeholder = @"";
    }
}
-(void)textFieldDidChange{
    NSString *text = tags.text;
    char last = [text characterAtIndex:text.length-1];
    
    [UIView animateWithDuration:.2 animations:^{
        form.contentOffset = CGPointMake(0, 160);
    }];
    
    if(last==',' && [tagList count]<=5){
        NSString *t = [text substringToIndex:text.length-1];
        tags.text = @"";
        [tagList addObject:t];
        
        Tag *tag = [[Tag alloc]initWithOrigin:CGPointMake(20, 210) andTagText:t];
        tag.alpha = 0;
        [form addSubview:tag];
        [UIView animateWithDuration:.2 animations:^{
            tag.alpha = 1;
        }];
        
        [tagButtonList insertObject:tag atIndex:0];
        for(int x=1; x<[tagButtonList count]; x++){
            Tag *curr = [tagButtonList objectAtIndex:x];
            
            [UIView animateWithDuration:.2 animations:^{
                
                curr.frame = CGRectMake(curr.frame.origin.x, curr.frame.origin.y+tag.frame.size.height+10, curr.frame.size.width, curr.frame.size.height);
                
            }];
        }
        
    }
    NSLog(@"%c", last);
}

-(IBAction)createPeg:(id)sender{
    if(!changed){
        [addP setText:@"Back"];
        
        form = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -210, self.view.frame.size.width, 223)];
        form.contentSize = CGSizeMake(form.frame.size.width, 203+(46.5*5));
        
        back = [[Tag alloc]initWithOrigin:CGPointMake(15, -50) andTagText:@"Back"];
        [self.view addSubview:back];
        
        create = [[Tag alloc]initWithOrigin:CGPointMake(15, -50) andTagText:@"Create"];
        create.frame = CGRectMake(self.view.frame.size.width-create.frame.size.width-15, -50, create.frame.size.width, create.frame.size.height);
        [self.view addSubview:create];
        
        UIButton *b = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [b addTarget:self
              action:@selector(createPeg:)
    forControlEvents:UIControlEventTouchUpInside];
        b.frame = CGRectMake(0, 0, back.frame.size.width, back.frame.size.height);
        [back addSubview:b];
        
        UIButton *c = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [c addTarget:self
              action:@selector(publishPeg:)
    forControlEvents:UIControlEventTouchUpInside];
        c.frame = CGRectMake(0, 0, create.frame.size.width, create.frame.size.height);
        [create addSubview:c];
        
        title = [[UITextField alloc]initWithFrame:CGRectMake(20, 0, form.frame.size.width-40, 40)];
        title.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"title" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        title.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        title.textColor = [UIColor whiteColor];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 42, title.frame.size.width, 1)];
        line.backgroundColor = [UIColor whiteColor];
        [form addSubview:line];
        
        [form addSubview:title];
        
        tags = [[UITextField alloc]initWithFrame:CGRectMake(20, 160, form.frame.size.width-40, 40)];
        tags.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"add a tag" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        tags.textColor = [UIColor whiteColor];
        tags.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        [tags addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
        
        UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(20, 202, title.frame.size.width, 1)];
        line2.backgroundColor = [UIColor whiteColor];
        [form addSubview:line2];
        
        [form addSubview:tags];
        
        placeholder = [[UITextField alloc]initWithFrame:CGRectMake(25, 50, form.frame.size.width-50, 40)];
        placeholder.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"provide a brief description" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        placeholder.userInteractionEnabled = false;
        placeholder.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        [form addSubview:placeholder];
        
        description = [[UITextView alloc]initWithFrame:CGRectMake(20, 50, form.frame.size.width-40, 100)];
        description.backgroundColor = [UIColor clearColor];
        description.font = placeholder.font;
        description.textColor = [UIColor whiteColor];
        description.delegate = self;
        description.font = [UIFont fontWithName:@"Avenir-Light" size:15];
        [form addSubview:description];
        
        
        [self.view addSubview:form];
        
        [UIView animateWithDuration:.65 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:0 options:0 animations:^{
            viewP.frame = CGRectMake(self.view.frame.size.width+50, viewP.frame.origin.y, viewP.frame.size.width, viewP.frame.size.height);
            viewC.frame = CGRectMake(0-viewC.frame.size.width-50, viewC.frame.origin.y, viewC.frame.size.width, viewC.frame.size.height);
            addP.frame = CGRectMake(0-addP.frame.size.width-50, addP.frame.origin.y, addP.frame.size.width, addP.frame.size.height);
            form.frame = CGRectMake(0, 60, form.frame.size.width, form.frame.size.height);
            back.frame = CGRectMake(15, 20, back.frame.size.width, back.frame.size.height);
            create.frame = CGRectMake(create.frame.origin.x, 20, create.frame.size.width, create.frame.size.height);
        }completion:^(BOOL completion){
            changed =true;
        }];
    }
    else{
        [addP setText:@"Create a Peg"];
        [self.view endEditing:YES];
        [form removeFromSuperview];
        form = nil;
        [UIView animateWithDuration:.65 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:0 options:0 animations:^{
            viewP.center =CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
            viewC.center = CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height/2)+45);
            addP.center = CGPointMake(self.view.frame.size.width/2, (self.view.frame.size.height/2)-45);
            form.frame = CGRectMake(0, -210, self.view.frame.size.width, 203);
            back.frame = CGRectMake(15, -50, back.frame.size.width, back.frame.size.height);
            create.frame = CGRectMake(create.frame.origin.x, -50, create.frame.size.width, create.frame.size.height);
        }completion:^(BOOL completion){
            changed =false;
        }];
    }
}
-(IBAction)viewPegs:(id)sender{
    UIViewController *viewPegs = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchPegs"];
    [self.navigationController pushViewController:viewPegs animated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)chats:(id)sender{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Chats"];
    [self.navigationController pushViewController:controller animated:YES];
}
-(IBAction)chat:(id)sender{
    
    Data *data = [[Data alloc]init];
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:username.text, @"userID", to.text, @"recipientID", chatid.text, @"chatID", chatTitle.text, @"title", [NSDate date], @"lastModified", nil];
    [data addChatWithInfo:dict];
    
    data = [[Data alloc]init];
    
    NSLog(@"%i", [[data getAllChats]count]);
    
    
    ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatController"];
    controller.selfID = username.text;
    controller.otherID = to.text;
    controller.chatID = chatid.text;
    [self.navigationController pushViewController:controller animated:YES];
     
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
