//
//  SearchPegsViewController.m
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 7/13/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import "SearchPegsViewController.h"

@interface SearchPegsViewController ()

@end

@implementation SearchPegsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    pegs = [[NSMutableDictionary alloc]init];
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    PFQuery *query = [PFQuery queryWithClassName:@"Peg"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            
            for (PFObject *object in objects) {
                [pegs setObject:object forKey:object.objectId];
            }
            
            [self setup];
            
            // Do something with the found objects
            for (PFObject *object in objects) {
                NSLog(@"%@", object.objectId);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}
-(void)setup{
    [scrollView removeFromSuperview];
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height*[pegs count]);
    [self.view addSubview:scrollView];
    
    NSArray *values = [pegs allValues];
    
    for(int x=0;x<[pegs count]; x++){
        
        PFObject *pegObject = [values objectAtIndex:x];
        
        UIView *peg = [[UIView alloc]initWithFrame:CGRectMake(0, x*self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        //191, 85, 236
        //244, 208, 63
        //46, 204, 113
        
        if(x%3==0){
            peg.backgroundColor = [UIColor colorWithRed:46.0/255.0 green:204.0/255.0 blue:113.0/255.0 alpha:1];
        }
        else if(x%3==1){
            peg.backgroundColor = [UIColor colorWithRed:244.0/255.0 green:208.0/255.0 blue:63.0/255.0 alpha:1];
        }
        else{
            peg.backgroundColor = [UIColor colorWithRed:191.0/255.0 green:85.0/255.0 blue:236.0/255.0 alpha:1];
        }
        [scrollView addSubview:peg];
        
        UITextView *pegTitle = [[UITextView alloc]initWithFrame:CGRectMake(30, 30, self.view.frame.size.width-60, 100)];
        pegTitle.backgroundColor = [UIColor clearColor];
        pegTitle.textColor = [UIColor whiteColor];
        pegTitle.text = [pegObject[@"title"] uppercaseString];
        pegTitle.font = [UIFont fontWithName:@"Avenir-Light" size:36];
        pegTitle.userInteractionEnabled = false;
        
        [peg addSubview:pegTitle];
        
        UITextView *pegMessage = [[UITextView alloc]initWithFrame:CGRectMake(30, 150, self.view.frame.size.width-60, 160)];
        pegMessage.backgroundColor = [UIColor clearColor];
        pegMessage.textColor = [UIColor whiteColor];
        pegMessage.text = pegObject[@"description"];
        pegMessage.font = [UIFont fontWithName:@"Avenir-Light" size:20];
        pegMessage.userInteractionEnabled = false;
        
        [peg addSubview:pegMessage];
        
        NSArray *tags = pegObject[@"tags"];
        
        Tag *t = [[Tag alloc]initWithOrigin:CGPointMake(30, 320) andTagText:[[tags valueForKey:@"description"] componentsJoinedByString:@", "]];
        
        [peg addSubview:t];
        
        
        if(!([pegObject[@"userID"] isEqualToString:[PFUser currentUser].objectId])){
            Tag *t2 = [[Tag alloc]initWithCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-50)andTagText:@"Pick up this Peg"];
            
            UIButton *pickUp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            
            pickUp.stringTag = pegObject.objectId;
            [pickUp addTarget:self action:@selector(pickUp:) forControlEvents:UIControlEventTouchUpInside];
            pickUp.frame = CGRectMake(0, 0, t.frame.size.width, t.frame.size.height);
            [t2 addSubview:pickUp];
            
            [peg addSubview:t2];
        }
        /*
        
        Tag *t3 = [[Tag alloc]initWithOrigin:CGPointMake(30, 320+t.frame.size.height+10) andTagText:@"Eminem"];
        [peg addSubview:t3];
        
        Tag *t4 = [[Tag alloc]initWithOrigin:CGPointMake(30+t3.frame.size.width+10, 320+t.frame.size.height+10) andTagText:@"Rap"];
        [peg addSubview:t4];
         */
    }

}
-(IBAction)pickUp:(id)sender{
    UIButton *but = sender;
    NSString *objectId = but.stringTag;
    PFObject *peg = [pegs objectForKey:objectId];
    NSString *recipientId = peg[@"userID"];
    
    PFObject *chatRoom = [PFObject objectWithClassName:@"ChatRoom"];
    chatRoom[@"members"] = [NSArray arrayWithObjects:[PFUser currentUser].objectId, recipientId, nil];
    [chatRoom saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSString *chatid = chatRoom.objectId;
            
            Data *data = [[Data alloc]init];
            NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[PFUser currentUser].objectId, @"userID", recipientId, @"recipientID", chatid, @"chatID", peg[@"title"], @"title", [NSDate date], @"lastModified", nil];
            [data addChatWithInfo:dict];
            
            ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatController"];
            controller.selfID = [PFUser currentUser].objectId;
            controller.otherID = recipientId;
            controller.chatID = chatid;
            [self.navigationController pushViewController:controller animated:YES];
            
            [peg deleteInBackground];
            
            NSString *message = [NSString stringWithFormat:@"Your Peg '%@' has been picked up!", peg[@"title"]];
            
            [PFCloud callFunctionInBackground:@"sendNotif"
                               withParameters:@{@"userID": recipientId,
                                                @"message": message}
                                        block:^(NSNumber *ratings, NSError *error) {
                                            if (!error) {
                                            }
                                        }];
            
        } else {
            // There was a problem, check error.description
        }
    }];
    
        
    NSLog(@"asdf %@", objectId);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
