//
//  Chat.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Chat : NSManagedObject

@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSString * recipientID;
@property (nonatomic, retain) NSString * chatID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * lastModified;

@end
