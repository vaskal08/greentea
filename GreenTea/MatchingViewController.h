//
//  MatchingViewController.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/14/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"
#import <Parse/Parse.h>
#import "ViewController.h"
#import "FLAnimatedImage.h"
#import "FLAnimatedImageView.h"

@interface MatchingViewController : UIViewController{
    NSTimer *timer;
    PFObject *matchToken;
    BOOL left;
}

@end
