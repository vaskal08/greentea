//
//  ViewController.h
//  College Chat
//
//  Created by Vasishta Kalinadhabhotla on 1/3/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageBubble.h"
#import <Parse/Parse.h>
#import <Sinch/Sinch.h>
#import <Sinch/SINMessageClient.h>
#import "Data.h"
#import "Chat.h"
#import <Firebase/Firebase.h>
#import "FLAnimatedImage.h"
#import "FLAnimatedImageView.h"

@interface ViewController : UIViewController<UIAlertViewDelegate, SINClientDelegate, SINMessageClientDelegate, UIScrollViewDelegate, UITextViewDelegate>{
    UIToolbar *toolbar;
    UITextField *textInput;
    UITextView *textView;
    CGRect keyboardRect;
    UIScrollView *scrollView;
    NSMutableArray *bubbles;
    NSDate *lastMessageFrom;
    CGFloat contentHeight;
    BOOL keyboardShowing;
    UIButton *send;
    id<SINClient> sinchClient;
    id<SINMessageClient> messageClient;
    int skip;
    BOOL loading;
    BOOL initialLoad;
    NSTimer *editor;
    bool editing;
    NSDate *startDate;
    FDataSnapshot *initialData;
    NSMutableArray *keys;
    BOOL end;
    
    FLAnimatedImageView *indicator;
    
    //tentative
}

@property NSString *selfID;
@property NSString *otherID;
@property NSString *chatID;
@property PFObject *chatRoom;
@property Chat *chat;

@end

