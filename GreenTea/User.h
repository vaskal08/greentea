//
//  User.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/8/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * userID;

@end
