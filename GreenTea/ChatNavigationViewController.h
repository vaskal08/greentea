//
//  ChatNavigationViewController.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/6/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <Parse/Parse.h>

@interface ChatNavigationViewController : UINavigationController

@property NSString *selfID;
@property NSString *otherID;
@property NSString *chatID;
@property PFObject *chatRoom;

@end
