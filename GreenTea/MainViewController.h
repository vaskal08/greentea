//
//  MainViewController.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 1/5/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatNavigationViewController.h"
#import "Data.h"
#import "Tag.h"
#import <Firebase/Firebase.h>
#import "ViewController.h"


@interface MainViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>{
    
    Tag *addP;
    Tag *viewP;
    Tag *viewC;
    Tag *back;
    Tag *create;
    
    UIScrollView *form;
    UITextField *placeholder;
    UITextView *description;
    
    UITextField *title;
    UITextField *tags;
    
    NSMutableArray *tagList;
    NSMutableArray *tagButtonList;
    
    
    BOOL changed;
}
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *to;
@property (weak, nonatomic) IBOutlet UITextField *chatid;
@property (weak, nonatomic) IBOutlet UITextField *chatTitle;

@end
