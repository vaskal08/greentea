//
//  ChatsViewController.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"
#import "Chat.h"
#import "ViewController.h"


@interface ChatsViewController : UIViewController{
    UIScrollView *scrollView;
    NSArray *chats;
}

@end
