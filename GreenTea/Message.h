//
//  Message.h
//  GreenTea
//
//  Created by Vasishta Kalinadhabhotla on 6/15/15.
//  Copyright (c) 2015 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSNumber * read;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSString * recipientID;
@property (nonatomic, retain) NSString * chatID;
@property (nonatomic, retain) NSDate * date;

@end
